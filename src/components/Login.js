import React, { useState } from 'react';

import './Login.scss'
import LoginForm from './LoginForm'

function Login() {
    const [submitDisabled, setSubmitDisabled] = useState(true);
    const [userEmail, setUserEmail] = useState('');
    const [rememberStatus, setRememberStatus] = useState(true);

    function emailUpdateHandler(e) {
        const email = e.target.value;
        setUserEmail(email);
        //TODO Do some validation here to userEmail and update below check 
        email === '' ? setSubmitDisabled(true) : setSubmitDisabled(false);
    }

    function rememberStatusUpdate() {
        setRememberStatus(!rememberStatus);
    }

    function submitHandler(e) {
        e.preventDefault();
        //TODO plugin to auth service
        alert('You submitted with email: ' + userEmail);
    }

    return (

        <div className='loginPod'>
            <a className='brandLogo' href='green.energy'>
                <img src='https://green.cdn.energy/branding/logo-r.svg' alt='link to homepage' />
            </a>
            <h1 className='withSub'>Example login screen</h1>
            <h2 className='subtitle'>Getting started with Green.</h2>
            <LoginForm
                submitDisabled={submitDisabled}
                emailUpdate={emailUpdateHandler}
                submitHandler={submitHandler}
                rememberEmailStatus={rememberStatus}
                rememberEmailUpdate={rememberStatusUpdate}
            />

        </div>
    );
}

export default Login;
