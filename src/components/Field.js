import React from 'react';

import './Field.scss';

function Field(props) {

    let returnField = '';

    if (props.type === 'checkbox') {
        returnField = (
            <div className='checkField'>
                <label className='fieldLabel'>
                    {props.label}

                    <input
                        className='fieldCheckbox'
                        type='checkbox'
                        onChange={props.updateHandler}
                        checked={props.checked}
                    />

                    <span className='customCheck'></span>
                </label>

            </div>
        );
    } else if (props.type === 'submit') {
        returnField = (
            <React.Fragment>
                <input
                    className='formSubmit'
                    type={props.type}
                    value={props.children}
                    disabled={(props.disabled) ? "disabled" : ""} />
            </React.Fragment>
        );
    } else {
        returnField = (
            <React.Fragment>
                {props.label ? <label className='fieldLabel'>{props.label}</label> : null}
                <input
                    className='fieldInput'
                    type={props.type}
                    onChange={props.updateHandler}
                    autoComplete={props.autocomplete} />
                {/* TODO add an error label and conditional styling if an invalid email is used */}
            </React.Fragment>
        );
    }

    return returnField;
}

export default Field;