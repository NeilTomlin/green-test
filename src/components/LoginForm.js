import React from 'react';

import './LoginForm.scss'
import Field from './Field';

function LoginForm(props) {
    return (
        <form className='loginForm' onSubmit={props.submitHandler}>
            <Field
                type='email'
                label='Email Address'
                updateHandler={(event) => props.emailUpdate(event)}
                autocomplete={props.rememberEmailStatus ? 'on' : 'off'}
            />
            <Field
                type='checkbox'
                label='Remember this device'
                checked={props.rememberEmailStatus}
                updateHandler={props.rememberEmailUpdate}
            />
            <Field
                type='submit'
                disabled={props.submitDisabled}>
                Sign In
            </Field>
        </form>
    );
}

export default LoginForm;